import { Router } from "express";
const candidateRouter = Router();
import fetchCandidateData from "../controllers/candidate-controller.js";

candidateRouter.get("/fetch-data", async (req, res) => {
  const cand = await fetchCandidateData();
  res.status(200).send(cand);
});

export default candidateRouter;
