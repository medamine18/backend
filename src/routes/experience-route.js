import { Router } from "express";
const experienceRouter = Router();
import fetchExperienceData from "../controllers/experience-controller.js";

experienceRouter.get("/fetch-data", async (req, res) => {
  const exp = await fetchExperienceData();
  res.status(200).send(exp);
});

export default experienceRouter;
