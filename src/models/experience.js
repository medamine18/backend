import mongoose from "mongoose";

const Schema = mongoose.Schema;
const experienceschema = new Schema({
  candidateId: String,
  companyName: String,
  title: String,
  startDate: Number,
  endDate: Number,
  summary: String
});

const experience = mongoose.model("workHistory", experienceschema);

export default experience;
