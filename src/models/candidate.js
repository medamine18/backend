import mongoose from "mongoose";
const Schema = mongoose.Schema;

const candidatschema = new Schema({
  id: Number,
  name: String,
  email: String,
  phone: String,
  description: String,
  occupation: String,
  address: {
    address1: String,
    city: String,
    state: String,
    zip: String,
    countryID: String
  },
  spokenLanguages: [],
  categories: [],
  skills: [],
  specialities: []
});
const candidate = mongoose.model("candidat", candidatschema);

export default candidate;
