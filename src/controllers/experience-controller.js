import experience from "../models/experience.js";

const fetchExperienceData = async () => {
  try {
    const experienceData = await experience
      .find({ candidateId: "603c07ff44e144a3cd281290" })
      .select({
        companyName: 1,
        title: 1,
        startDate: 1,
        endDate: 1,
        summary: 1,
        _id: 0
      });
    return experienceData;
  } catch (e) {
    console.log("-------------", e);
  }
};

export default fetchExperienceData;
