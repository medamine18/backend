import candidate from "../models/candidate.js";

const fetchCandidateData = async () => {
  try {
    //const c =  new candidatTable();
    //await c.save({id: 2});

    const candidateData = candidate.find({ id: 128646 }).select({
      id: 1,
      name: 1,
      email: 1,
      phone: 1,
      occupation: 1,
      "address.address1": 1,
      "address.city": 1,
      "address.state": 1,
      "address.zip": 1,
      "address.countryID": 1,
      spokenLanguages: 1,
      categories: 1,
      skills: 1,
      specialties: 1,
      _id: 0
    });
    return candidateData;
  } catch (e) {
    console.log("-------------", e);
  }
};

export default fetchCandidateData;
